import { createNativeStackNavigator, useColorScheme, Button } from '@react-navigation/native-stack';
import Homepage from "../pages/home"
import Products from "../pages/products";
import React, { useEffect, useState } from "react";
import ProductDetail from '../pages/product/product';
import { useDispatch, useSelector } from 'react-redux';
import { getToken, setUser } from '../store/users';
const Stack = createNativeStackNavigator()
import Model from '../models/model';
import axios from 'axios';
import Login from '../pages/login';
import MyAccount from '../pages/my-account';

export default function RootStactNavigator() {
   const dispatch = useDispatch()
   const user = useSelector(state=> state.users.user)
   const [retryToken, setRetryToken] = useState(3)

   const getToken = async () => {
      return await axios({
         method: 'post',
         url: encodeURI(`${Model.apiUrl}/oauth/token`),
         headers: {
            'Authorization': `Bearer`
         },
         data: {
            grant_type: 'refresh_token',
            client_id: 2
         }
      })
      .catch((e) => {
         if (user.token == '') {
            dispatch(setUser({
               ...user,
               token: e.response.data.noauth_token
            }))
         }
      })
   }

   useEffect(()=> {
      getToken()
   },[])

   return (
      <Stack.Navigator 
         initialRouteName="Home"
      >
         <Stack.Screen name="Home" component={Homepage} options={{ headerShown: false }}/>
         <Stack.Screen name="Products" component={Products} options={{ headerShown: false }}/>
         <Stack.Screen name="Product" component={ProductDetail} />
         <Stack.Screen name="Login" component={Login} />
         <Stack.Screen name="MyAccount" component={MyAccount} />
      </Stack.Navigator>
   )
}