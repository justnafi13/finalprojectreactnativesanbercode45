import Color from "./color"

class Image{
	constructor(image_json){
      image_json = image_json ?? {}
      
		this.uuid = image_json.uuid ?? ''
		this.img_url = image_json.img_url ?? ''
		this.thumbs = image_json.thumbs ?? {}
		this.color = new Color(image_json.color)
	}
}

export default Image