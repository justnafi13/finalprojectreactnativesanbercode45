import ProductVariant from "./product-variant"
import Color from "./color"
import Image from "./image"
import axios from "axios"
import Model from "./model"

class Product {
	constructor(product_json) {
		this.uuid = product_json.uuid ?? ''
		this.name = product_json.name ?? ''
		this.description = product_json.description
		this.variants = product_json.variants ? ProductVariant.jsonToVariants(product_json.variants) : []
		this.prev_image = product_json.prev_image ? new Image(product_json.prev_image) : []
		this.price = product_json.price ?? 0
		this.slug = product_json.slug ?? ''
		this.isNew = product_json.is_new ?? false
		this.isSoldout = product_json.is_soldout ?? false
		this.isLowStock = product_json.is_low_stock ?? false
		this.sizeGuideImg = product_json.size_guide_img ?? ''
		this.colors = product_json.colors ? product_json.colors.map(c => new Color(c)) : []
		this.images = product_json.images ? product_json.images.map(i => new Image({ ...i, ...i.colors })) : []
		this.price_normal = product_json.price_normal ?? null
		this.shipping_return = product_json.shipping_return ?? ''
    	this.flash_sale = product_json.flash_sale ?? null
	}

   static async fetchBySlug(slug) {
		return await axios({
			method: 'get',
			url: encodeURI(`${Model.apiUrl}/products/${slug}`),
		})
			.then(response => {
				return response.data
			})
			.catch((e) => {
				return {
					'success': false,
					'error_message': e,
					'product': null
				}
			})
	}
}

export default Product