import Color from "./color"

class ProductVariant {
	constructor(variant_json){
		this.uuid = variant_json.uuid ?? ''
		this.name = variant_json.name
		this.price = variant_json.price
		this.max = variant_json.max
		this.color = new Color(variant_json.color)
		this.priceNormal = variant_json.price_normal
	}

   static jsonToVariants(variants_json){
		try {
			return variants_json.map(variant=>new ProductVariant(variant))
		} catch (e) {
			console.warn(e)
			return []
		}
	}
}

export default ProductVariant