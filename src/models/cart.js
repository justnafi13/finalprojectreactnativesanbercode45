import axios from "axios"
import Model from "./model"

class Cart {
   static async addProduct(product, qty, token) {
		return await axios({
			method: 'post',
			url: encodeURI(`${Model.apiUrl}/cart`),
			headers: {
				'Accept': 'application/json',
				'Authorization': `Bearer ${token}`,
			},
			data: {
				"uuid": product,
				"qty": qty,
			}
		})
		.then((r)=>{
			return r.data
		})
		.catch((e)=>console.log(e))
	}
}

export default Cart