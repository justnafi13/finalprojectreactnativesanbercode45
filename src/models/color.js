class Color {
	constructor(color_json){
      color_json = color_json ?? {}
      
		this.uuid = color_json.uuid ?? ''
		this.name = color_json.name ?? 'default'
		this.hex = color_json.hex ?? '#FFFF'
	}
}

export default Color