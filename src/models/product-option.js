class ProductOption{	
	constructor(json_option) {
		this.id = json_option.id ?? ''
		this.name = json_option.name ?? ''
		this.sid =  json_option.uuid ?? ''
		this.slug = json_option.slug ?? ''
	}
}

export default ProductOption