class User {
	constructor(user_json = {}) {
		this.email = user_json.email ?? ''
		this.name = user_json.name ?? ''
		this.mobile = user_json.mobile ?? ''
		this.address = user_json.address ?? ''
		this.country = user_json.country ?? 'indonesia'
		this.province = user_json.province ?? ''
		this.city = user_json.city ?? ''
		this.district = user_json.district ?? ''
		this.postal_code = user_json.postal_code ?? ''
		this.dob = user_json.dob ?? ''
		this.salutation = user_json.salutation ?? ''
		this.token = user_json.token ?? ''
		this.isLogin = user_json.isLogin ?? false
	}
}

export default User