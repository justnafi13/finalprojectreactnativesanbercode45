import { SafeAreaView, ScrollView, View, StyleSheet } from "react-native";
import Aside from "../components/Aside";
import Header from "../components/Header";
import { useSelector, useDispatch } from "react-redux"

export default function LayoutDefault({children}) {
   const hamburger = useSelector((state) => state.main.hamburger )

   return (
      <SafeAreaView className={`flex-1 flex flex-row flex-nowrap relative  ${hamburger ? 'bg-[#F6F8F2]' : 'bg-gray-50'}`}>
         <Aside />
         <View className={`flex w-full relative ${hamburger ? 'mt-10 bg-white rounded-3xl overflow-hidden' : ''}`}>
            <Header />
            {children}
         </View>
      </SafeAreaView>
   )
}

const style = StyleSheet.create({
   position: {
      transform: [
         {
            translateX: -100
         },
      ]
   }
})