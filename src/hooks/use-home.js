import { useState, useEffect, useCallback } from "react";
import Setting from "../models/setting";
import axios from 'axios'
import Model from "../models/model";

const useHome = () => {
    const [fetchedHome, setFechedHome] = useState({
         features: [],
         slideshows: [],
         isHomeLoading: true,
         error: false,
    })

    const fecthHome = useCallback(async() => {
      await axios({
         method: 'get',
         url: `${Model.apiUrl}/home`,
       })
      .then((res) => {
         if (res.data.success) {
            setFechedHome({
               features: res.data.features,
               slideshows: res.data.slideshows,
               isHomeLoading: false,
               error: false,
            })
         }else{
            setFechedHome({
               ...fetchedHome,
               error: true
            })
         }
      })
      .catch((err)=>console.log(err,'err'))
    })

    useEffect(()=>{
      fecthHome();
    },[setFechedHome])

    const { features, slideshows, isHomeLoading, error } = fetchedHome

    return { features, slideshows, isHomeLoading, error };
}

export default useHome;