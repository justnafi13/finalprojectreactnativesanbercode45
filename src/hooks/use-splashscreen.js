import React, { useCallback, useEffect, useState } from "react";
import * as SplashScreen from 'expo-splash-screen';

SplashScreen.preventAutoHideAsync();
export default function useSplashScreen() {
	const [appIsReady, setAppIsReady ] = useState(false)

	const prepare = async () => {
		try {
			await new Promise(resolve => setTimeout(resolve, 2000));
		} catch (e) {
			console.warn(e);
		} finally {
			setAppIsReady(true);
		}
	}

	useEffect(()=>{
		prepare()
	},[])

	return { appIsReady }
}