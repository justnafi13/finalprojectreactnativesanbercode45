import { useState, useEffect, useCallback } from "react";
import Setting from "../models/setting";
import axios from 'axios'
import Model from "../models/model";
import ProductOption from "../models/product-option";

const useCategory = () => {
    const [fetchedProductOption, setFetchedProductOption] = useState({
         categoris: [],
         sizes: [],
         isCatLoading: true,
         error: false,
    })

    const fecthProductOption = useCallback(async() => {
      await axios({
         method: 'get',
         url: `${Model.apiUrl}/products/filters`,
       })
      .then((r) => {
         if(r.data.success){
				r.data.categories = r.data.categories.map((o)=>new ProductOption(o))
				r.data.sizes = r.data.sizes.map((o)=>new ProductOption(o))
				
            setFetchedProductOption({
               categoris: r.data.categories,
               sizes: r.data.sizes,
               isCatLoading: false,
               error: false,
            })

         }else{
            setFetchedProductOption({
               ...fetchedProductOption,
               error: true
            })
         }
      })
      .catch((err)=>console.log(err,'err'))
    })

    useEffect(()=>{
      fecthProductOption();
    },[setFetchedProductOption])

    const { categoris, sizes, isCatLoading, error } = fetchedProductOption

    return { categoris, sizes, isCatLoading, error };
}

export default useCategory;