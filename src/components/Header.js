import { View, Text, Image, TouchableOpacity, TextInput } from "react-native";
import IconBurger from "../assets/svg/icon_burger_menu.svg"
import IconSearch from "../assets/svg/icon_search.svg"
import IconCart from "../assets/svg/icon_cart.svg"
import { useSelector, useDispatch } from "react-redux"
import { doHamburger } from "../store/main";
import { useNavigation } from '@react-navigation/native';

export default function Header() {
   const hamburger = useSelector((state) => state.main.hamburger )
   const cartItem = useSelector((state) => state.cart.cart.items)
   const dispatch = useDispatch()

   return (
      <View className={`Header flex flex-row items-center ${hamburger ? 'pt-4 pb-2' : 'py-2'}`}>
         <TouchableOpacity className="px-4" onPress={()=> dispatch(doHamburger(!hamburger))}>
            <IconBurger width={20} height={20}  fill="#000" />
         </TouchableOpacity>
         <View className="flex-auto relative flex justify-center items-start">
            <TextInput 
               className="rounded-md border-b-gray-900 border bg-gray-200 dark:bg-gray-900 dark:text-white pl-8 pr-2  py-2 w-full"
               placeholder="Search"
            />
            <View className="absolute left-2">
               <IconSearch width={15} height={15} fill="#000" />
            </View>
         </View>
         <TouchableOpacity className="px-4 relative flex items-center justify-center">
            <IconCart width={24} height={24}  fill="#000" />
            <Text className="absolute top-2.5 text-[9px]">{cartItem.length}</Text>
         </TouchableOpacity>
      </View>
   )
}