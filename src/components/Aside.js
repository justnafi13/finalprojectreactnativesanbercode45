import { 
   View,
   Text,
   TouchableOpacity
} from "react-native";
import { useSelector, useDispatch } from "react-redux"
import IconHome from "../assets/svg/icon_home.svg"
import IconProduct from "../assets/svg/icon_product.svg"
import IconAccount from "../assets/svg/icon_account.svg"
import IconAccountProfile from "../assets/svg/icon_account_profile.svg"
import { useNavigation } from '@react-navigation/native';
import { doReqOptions, fetchProducts } from "../store/products";
import { doHamburger } from "../store/main";

export default function Aside() {
   const hamburger = useSelector((state) => state.main.hamburger)
   const user = useSelector((state) => state.users.user)
   const navigation = useNavigation();
   const dispatch = useDispatch()

   const doProducts = () => {
      dispatch(doHamburger(!hamburger))
      navigation.navigate('Products')
   }

   return (
      <View className={`w-2/3  ${hamburger ? 'flex flex-col pt-10' : 'hidden'}`}>
         {
            user.isLogin ? 
            <View className="flex justify-center items-center flex-col px-2 space-y-4">
               <View>
                  <IconAccount width={30} height={30} />
               </View>
               <Text className="text-md font-medium">Hi {user.name}</Text>
            </View> :
            <View className="flex justify-center items-center flex-col px-2 space-y-4">
               <Text className="text-base font-semibold">Daftar Sekarang</Text>
               <Text className="text-center text-xs">
                  Login atau buat account untuk kemudahan lacak pesanan dan proses checkout yang cepat.
               </Text>
               <TouchableOpacity onPress={()=> navigation.navigate('Login')} className="px-4 py-2 rounded-lg bg-primary shadow-md">
                  <Text className="uppercase text-gray-50">Login or Register</Text>
               </TouchableOpacity>
            </View>
         }
         <View className="pt-10">
            <TouchableOpacity onPress={() => navigation.navigate('Home')} className="flex flex-row space-x-3 items-center px-4 py-2">
               <IconHome width={23} height={23} />
               <Text className="text-md text-semibold">Home</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => doProducts() } className="flex flex-row space-x-3 items-center px-4 py-2">
               <IconProduct width={23} height={23} />
               <Text className="text-md text-semibold">Products</Text>
            </TouchableOpacity>
            {
               user.isLogin &&
               <TouchableOpacity onPress={() => navigation.navigate('MyAccount') } className="flex flex-row space-x-3 items-center px-4 py-2">
                  <IconAccountProfile width={23} height={20} />
                  <Text className="text-md text-semibold">Account</Text>
               </TouchableOpacity>
            }
         </View>
      </View>
   )
}