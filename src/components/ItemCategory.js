import { TouchableOpacity, Text } from "react-native"
import { useSelector, useDispatch } from "react-redux"
import { doReqOptions, fetchProducts } from "../store/products";

export default function ItemCategory({item}) {
   const reqOptions = useSelector((state) => state.products.reqOptions )
   const dispatch = useDispatch();

   const doCategories = (item) => {
      const req = {
         ...reqOptions,
         categories: reqOptions.categories != item.slug ? item.slug : ''
      }
      dispatch(doReqOptions(req))
      dispatch(fetchProducts(req))
   }

   return (
      <TouchableOpacity 
         onPressIn={() => doCategories(item)}
         className={`rounded-2xl border border-gray-700 shadow-md flex items-center justify-center ml-3 ${reqOptions.categories == item.slug ? 'bg-primary' : 'bg-[#F6F8F2]'}`}
      >
         <Text className={`py-1.5 px-4 ${reqOptions.categories == item.slug ? 'text-white' : 'text-gray-800'}`}>{item.name}</Text>
      </TouchableOpacity>
   )
}