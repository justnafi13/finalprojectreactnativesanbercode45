import {
   TouchableOpacity,
   Text,
   Image
} from "react-native"
import { useNavigation } from '@react-navigation/native';

export default function ItemProduct({item, index}) {
   const navigation = useNavigation();

   return (
      <TouchableOpacity onPress={()=> navigation.navigate('Product', { slug: item.slug})} className={`flex-1/2 w-1/2 relative pb-4 ${index % 2 !== 0 ? 'pr-0' : 'pr-2'}`}>
         <Image 
            source={{
               uri: item.prev_image.img_url,
            }}
            className={`w-full h-72 rounded-xl`}
            resizeMode={"cover"}
            alt={item.name}
         />
         <Text className="mt-2">{item.name}</Text>
      </TouchableOpacity>
   )
}