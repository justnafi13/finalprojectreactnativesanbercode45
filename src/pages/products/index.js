import { View, Text, useColorScheme, TouchableOpacity, FlatList } from "react-native";
import React, { useEffect } from "react";
import { StatusBar } from 'expo-status-bar';
import LayoutDefault from "../../layouts/defaults";
import IconFilter from "../../assets/svg/icon_filter.svg"
import useCategory from "../../hooks/use-category";
import ItemCategory from "../../components/ItemCategory";
import { useSelector, useDispatch } from "react-redux"
import { fetchProducts } from "../../store/products";
import ItemProduct from "../../components/ItemProduct";

export default function Products({route, navigation}) {
   const isDarkMode = useColorScheme() === 'dark';
   const { categoris , sizes, isCatLoading, error } = useCategory()
   const reqOptions = useSelector((state) => state.products.reqOptions )
   const products = useSelector((state) => state.products.products )
   const dispatch = useDispatch();

   useEffect(()=>{

      let req = {
         categories: '',
         sizes: '',
         sortby: 'product_newest',
         search: '',
         page: 1
      }

      dispatch(fetchProducts(req))
   },[])

   return (
      <LayoutDefault>
         <View className="px-4 py-2 flex relative justify-between flex-row items-center">
            <FlatList
               horizontal
               data={categoris}
               keyExtractor={(item, index) => index.toString()}
               renderItem={({item, index}) => <ItemCategory item={item} />}
            />
            <TouchableOpacity className="bg-gray-50 pl-2">
               <IconFilter width={23} height={23} fill="#000" />
            </TouchableOpacity>
         </View>
         <View className="flex-1 px-4 items-center justify-center">
            {
               products.isLoading ? 
               <Text className="text-center">....Loading</Text> :
               (
                  products.data && products.data.items.length > 0 ? 
                  <FlatList
                     numColumns={2}         
                     data={products.data.items}
                     keyExtractor={(item, index) => index.toString()}
                     renderItem={({item, index}) =><ItemProduct item={item} index={index} />}
                  /> :
                  <Text className="text-center">Data tidak ada</Text>
               )
            }
         </View>
      </LayoutDefault>
   )
}