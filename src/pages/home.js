import React, { useEffect } from "react";
import { View, Text, SafeAreaView, FlatList, Image } from "react-native";
import useHome from "../hooks/use-home";
import LayoutDefault from "../layouts/defaults";
import { Setting } from "../models/setting";

export default function Homepage() {
   const { features, slideshows, isHomeLoading, error } = useHome()

   const Item = ({item}) => (
      <View className="flex mt-2">
        <Image 
            source={{
               uri: item.image_mobile,
            }}
            className="w-full h-72 rounded-xl"
            resizeMode={"cover"}
            alt={item.caption}
        />
      </View>
   );

   return (
      <LayoutDefault>
         {
            isHomeLoading && features.length < 0 ?
            <View className="flex px-4 space-y-2">
               {
                  Array(4).fill('zull').map((item, i) => (
                     <View className="w-full h-60 rounded-xl bg-gray-200 animate-pulse"></View>
                  ))
               }
            </View> :
            <View className="px-4">
               <FlatList
                  vertical
                  data={features}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item, index}) => <Item item={item} />}
               />
            </View>
         }
      </LayoutDefault>
   )
}