import { useEffect } from "react";
import { 
   View,
   Text,
   TouchableOpacity,
   SafeAreaView
} from "react-native"
import { useSelector } from "react-redux"
import IconCart from "../../assets/svg/icon_cart.svg"
import IconBack from "../../assets/svg/icon_back.svg"

export default function MyAccount({route, navigation}) {
   const user = useSelector((state)=>state.users.user)
   const cartItem = useSelector((state) => state.cart.cart.items)

   const headerCustom = () => {
      navigation.setOptions({
         headerTransparent: true,
         title: 'My Account',
         headerLeft: () => (
            <TouchableOpacity onPress={()=> navigation.navigate('Products')} className="relative flex items-center justify-center">
               <IconBack width={24} height={20}  fill="#000" />
            </TouchableOpacity>
         ),
         headerRight: () => (
            <TouchableOpacity className="relative flex items-center justify-center">
               <IconCart width={24} height={24}  fill="#000" />
               <Text className="absolute top-2.5 text-[9px]">{ cartItem.length }</Text>
            </TouchableOpacity>
         ),
      });
   }

   useEffect(()=>{
      headerCustom()
   },[cartItem])
   return (
      <SafeAreaView className="flex-1 items-center justify-center">
         <View className="border border-primary rounded-3xl px-5 py-10">
            <Text>Name: {user.name}</Text>
         </View>
      </SafeAreaView>
   )
}