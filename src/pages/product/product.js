import { useNavigation } from "@react-navigation/native";
import { useEffect, useState } from "react";
import { 
   View,
   Text,
   Button,
   ScrollView,
   TouchableOpacity,
   FlatList,
   Image,
   ActivityIndicator,
   Dimensions
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import IconCart from "../../assets/svg/icon_cart.svg"
import IconBack from "../../assets/svg/icon_back.svg"
import IconAccordion from "../../assets/svg/icon_accordion.svg"
import Product from "../../models/product";
import { useSelector, useDispatch } from "react-redux";
import { setCart } from "../../store/cart";
import Cart from "../../models/cart";

export default function ProductDetail({route, navigation}) {
   const cartItem = useSelector((state) => state.cart.cart.items)
   const user = useSelector((state) => state.users.user)
   const dispatch = useDispatch()
   const [product, setProduct] = useState({})
   const [loading, setLoading] = useState(true)
   const [count, setCount] = useState(1)
   const { slug } = route.params
   const windowWidth = Dimensions.get('window').width;
   const windowHeight = Dimensions.get('window').height;
   
   const fetchProduct = async () => {
      try {
         const res = await Product.fetchBySlug(slug)
         if (res.success) {
            console.log(res.product.variants[0].price_format)
            setProduct(res.product)
            setLoading(false)
         }
      } catch (error) {}
   }

   const countQty = cartItem.reduce((a, { qty}) => {
      return a + qty
   }, 0)

   const headerCustom = () => {
      navigation.setOptions({
         headerTransparent: true,
         title: '',
         headerLeft: () => (
            <TouchableOpacity onPress={()=> navigation.navigate('Products')} className="relative flex items-center justify-center">
               <IconBack width={24} height={20}  fill="#000" />
            </TouchableOpacity>
         ),
         headerRight: () => (
            <TouchableOpacity className="relative flex items-center justify-center">
               <IconCart width={24} height={24}  fill="#000" />
               <Text className="absolute top-2.5 text-[9px]">{ cartItem.length }</Text>
            </TouchableOpacity>
         ),
      });
   }

   const increment = () =>{
      setCount(count + 1)
   }

   const decrement = () =>{
      if (count <= 0 ) return;
      setCount(count - 1)
   }

   const addToCart = async (item) => {
      let addedItem = await Cart.addProduct(
         item.variants[0].uuid,
         count,
         user.token
      )
      
      if (addedItem.success) {
         dispatch(setCart(
            addedItem.cart
         ))
      }

   }

   useEffect(()=>{
      headerCustom()
      fetchProduct()
   },[navigation, cartItem])

   return (
      <View className="flex-1 relative">
         {
            loading ? 
            <ActivityIndicator size="small" color="#0000ff" /> :
            <>
               <ScrollView className="flex-1">
                  <FlatList
                     horizontal
                     className="flex-1 flex flex-row w-full h-2/3 bg-red-300"
                     data={product.images}
                     keyExtractor={(item, index) => index.toString()}
                     renderItem={({item, index}) =>
                        <Image 
                           source={{
                              uri: item.img_url,
                           }}
                           style={{width: windowWidth, height: windowHeight - (windowHeight/3)}}
                           className={`flex-1 h-72`}
                           resizeMode={"cover"}
                           alt={item.uuid}
                        />
                     }
                  />
                  <View className="mt-2 mb-4 mx-[18px]">
                     <Text className="text-xl font-bold">{product.name}</Text>
                     <Text className="text-base font-light text-gray-700">IDR { product.variants[0].price_format }</Text>
                  </View>
                  <View className="my-2 mx-[18px] border-y border-gray-400">
                     <TouchableOpacity className="flex flex-row justify-between items-center py-2">
                        <Text className="font-normal">Detail</Text>
                        <IconAccordion width={13} height={13}  fill="#000" />
                     </TouchableOpacity>
                     <View className="flex flex-col pb-2">
                        <Text>Hello</Text>
                     </View>
                  </View>
               </ScrollView>
               <View className="absolute bottom-0 left-0 right-0 flex-1 bg-[#F6F8F2] pt-4 pb-8">
                  <View className="px-5 flex flex-row justify-between">
                     <View className="flex flex-row space-x-4 justify-around">
                        <TouchableOpacity onPress={()=>decrement()} className="py-2 px-3 flex items-center justify-center rounded-xl bg-primary shadow-lg">
                           <View className="w-5 h-1 bg-[#F6F8F2]"></View>
                        </TouchableOpacity>
                        <View className="flex items-center justify-center py-2 px-3">
                           <Text className="uppercase text-base font-semibold">{count}</Text>
                        </View>
                        <TouchableOpacity onPress={()=>increment()}  className="py-2 px-5 flex items-center justify-center rounded-xl bg-primary shadow-lg">
                           <View className="w-5 h-1 bg-[#F6F8F2] absolute"></View>
                           <View className="w-1 h-5 bg-[#F6F8F2]"></View>
                        </TouchableOpacity>
                     </View>
                     <TouchableOpacity onPress={()=> addToCart(product)} className="px-6 py-2 rounded-lg bg-primary shadow-lg">
                        <Text className="uppercase text-base font-semibold text-[#F6F8F2]">Add to Cart</Text>
                     </TouchableOpacity>
                  </View>
               </View>
            </>
         }
      </View>
   )
}