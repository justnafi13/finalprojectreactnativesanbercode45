import { 
   View,
   TouchableOpacity,
   Text,
   SafeAreaView,
   TextInput
} from "react-native";
import IconCart from "../assets/svg/icon_cart.svg"
import IconBack from "../assets/svg/icon_back.svg"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { doLogin } from "../store/users";

export default function Login({route, navigation}) {
   const cartItem = useSelector(state=> state.cart.cart.items)
   const loading = useSelector(state=> state.users.loading)
   const status = useSelector(state=> state.users.status)
   const user = useSelector(state=> state.users.user)
   const dispatch = useDispatch()
   const [ formLogin, setFromLogin ] = useState({
      email: '',
      password: ''
   })

   const headerCustom = () => {
      navigation.setOptions({
         headerTransparent: true,
         title: 'ZULL',
         headerLeft: () => (
            <TouchableOpacity onPress={()=> navigation.navigate('Products')} className="relative flex items-center justify-center">
               <IconBack width={24} height={20}  fill="#000" />
            </TouchableOpacity>
         ),
         headerRight: () => (
            <TouchableOpacity className="relative flex items-center justify-center">
               <IconCart width={24} height={24}  fill="#000" />
               <Text className="absolute top-2.5 text-[9px]">{ cartItem.length }</Text>
            </TouchableOpacity>
         ),
      });
   }
   
   const login = async () => {
      dispatch(
         doLogin({
            email: formLogin.email,
            password: formLogin.password,
            token: user.token
         })
      )

      if (status) {
         navigation.navigate('Products')
      }
   }

   useEffect(()=> {
      headerCustom()
   },[])

   return (
      <SafeAreaView className="flex-1">
         <View className="flex-1 my-5">
            <View className="flex flex-col items-center justify-center">
               <Text className="uppercase text-xl font-semibold">Login</Text>
               <Text className="text-md">Silahkan masuk kedalam akun kamu</Text>
            </View>
            <View className="FormLogin flex flex-col items-center justify-center py-10 px-5 space-y-4">
               <View className="w-full">
                  <Text>Email</Text>
                  <TextInput autoCapitalize="none" autoCorrect={false} textContentTyp="emailAddress" keyboardType='email-address' autoCompleteType='email' className="h-10 border-b" placeholder="Alamat Email" value={formLogin.email} onChangeText={(value)=> setFromLogin({...formLogin, email: value})}/>
               </View>
               <View className="w-full">
                  <Text>Password</Text>
                  <TextInput secureTextEntry={true} className="h-10 border-b" placeholder="Password" value={formLogin.password} onChangeText={(value)=> setFromLogin({...formLogin, password: value})}/>
               </View>
               <TouchableOpacity onPress={()=> login()} className="bg-primary rounded-lg px-6 py-2">
                  <Text className="text-white text-base font-semibold uppercase">{ loading ? '..loading' : 'Login'}</Text>
               </TouchableOpacity>
            </View>
         </View>
      </SafeAreaView>
   )
}