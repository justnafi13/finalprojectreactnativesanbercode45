import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	cart: {
      items: [],
      promo: {},
      shipment: null
   },
}

export const cartSlice = createSlice({
	name: "cart",
	initialState,
	reducers: {
		setCart: (state, action) => {
         state.cart.items = action.payload
		},
	},
})

export const { setCart } = cartSlice.actions
export default cartSlice.reducers
