import { configureStore } from "@reduxjs/toolkit"
import { mainSlice } from "../store/main"
import { cartSlice } from "./cart"
import { productsSlice } from "./products"
import { userSlice } from "./users"


export const store = configureStore({
	reducer: {
		[mainSlice.name]: mainSlice.reducer,
		[productsSlice.name]: productsSlice.reducer,
		[cartSlice.name]: cartSlice.reducer,
		[userSlice.name]: userSlice.reducer,
	},
	middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({serializableCheck: false}),
})
