import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from "axios"
import Product from "../models/product"
import Model from "../models/model"

const initialState = {
	reqOptions: {
      categories: '',
      sizes: '',
      sortby: 'product_newest',
      search: '',
      page: 1
   },
   products: {
      isLoading: false,
      data: null,
      error: false
   }
}

export const fetchProducts = createAsyncThunk("fetchProducts", async (params) => {
   return await axios({
      method: 'get',
      url: encodeURI(`${Model.apiUrl}/products`),
      params: params,
   })
   .then(response => {
      return response.data.products
   })
   .catch((e) => console.warn(e))
})

export const productsSlice = createSlice({
	name: "products",
	initialState,
	reducers: {
		doReqOptions: (state, action) => {
         state.reqOptions = action.payload
		},
	},
   extraReducers: (builder) => {
      builder.addCase(fetchProducts.pending, (state, action) => {
         state.products.isLoading = true
      })
      builder.addCase(fetchProducts.fulfilled, (state, action) => {
         state.products.isLoading = false
         state.products.data = action.payload
      })
      builder.addCase(fetchProducts.rejected, (state, action) => {
         state.products.error = true
      })
   }
})

export const { doReqOptions } = productsSlice.actions
export default productsSlice.reducers
