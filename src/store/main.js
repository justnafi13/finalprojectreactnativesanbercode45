import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	hamburger: false,
}

export const mainSlice = createSlice({
	name: "main",
	initialState,
	reducers: {
		doHamburger: (state, action) => {
         state.hamburger = action.payload
		},
	},
})

export const { doHamburger } = mainSlice.actions
export default mainSlice.reducers
