import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import User from "../models/user"
import Model from "../models/model"
import axios from "axios"

const initialState = {
	user: new User(),
   loading: false,
   error: false,
   status: false
}

export const doLogin = createAsyncThunk("doLogin", async (userObj) => {
   console.log(userObj,'store')
   let login = await axios({
      method: 'post',
      url: encodeURI(`${Model.apiUrl}/auth/login`),
      headers: {
         'Authorization': `Bearer ${userObj.token}`
      },
      data: {
         email: userObj.email,
         password: userObj.password,
      }
   })
   .then((r) => {
      console.log(r,'ress')
      try {
         if(r.data.country && r.data.country.length < 0) {
            r.data.country = 'indonesia'
         }
      } catch (error) {
         
      }
      return r.data
   })
   .catch((e)=>console.log(e,'err'))

   try {
      if (login.success) {
         let user = {
            ...login.user,
            isLogin: true,
            token: login.token
         }
         login.user = new User(user)
      }
   } catch (error) {
      login.success = false
      login.errors = { general: 'an error occured' }
   }
   
   return login;
})

export const userSlice = createSlice({
	name: "users",
	initialState,
	reducers: {
		setUser: (state, action) => {
         state.user = action.payload
		},
	},
   extraReducers: (builder) => {
      builder.addCase(doLogin.pending, (state, action) => {
         state.loading = true
      })
      builder.addCase(doLogin.fulfilled, (state, action) => {
         state.loading = false
         state.status = true
         state.user = action.payload.user
      })
      builder.addCase(doLogin.rejected, (state, action) => {
         state.error = true
      })
   }
})

export const { setUser } = userSlice.actions
export default userSlice.reducers
