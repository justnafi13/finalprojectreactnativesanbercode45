import { 
  StatusBar,
  useColorScheme
} from 'react-native';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

import useSplashScreen from './src/hooks/use-splashscreen';
import * as SplashScreen from 'expo-splash-screen';
import React, { useCallback, useEffect, useState } from 'react';
import RootStactNavigator from './src/routes';
import { NavigationContainer } from '@react-navigation/native';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { store } from './src/store';

export default function App() {
  const isDarkMode = useColorScheme() === 'dark';
  const { appIsReady } = useSplashScreen()
  
  const onLayoutRootView = useCallback( async ()=> {
    if (appIsReady) {
      await SplashScreen.hideAsync();
    }
  },[appIsReady])

  if(!appIsReady) {
    return null;
  }

  return (
    <Provider store={store}>
      <NavigationContainer onReady={onLayoutRootView}>
        <RootStactNavigator />
      </NavigationContainer>
    </Provider>
  );
}
